import React, { useState } from "react";

const Login = () => {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");

  return (
    <div>
      <div className="login">
        <h2 className="active"> sign in </h2>
        <h2 className="nonactive"> sign up </h2>
        <form>
          <input
            type="text"
            className="text"
            name="username"
            value={userName}
            onChange={(e) => {
              setUserName(e.target.value);
            //   console.log(e.target.value);
            }}
          />
          <span style={{ color: "white" }}>username</span>
          <br />
          <br />
          <input
            type="password"
            className="text"
            name="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            //   console.log(e.target.value);
            }}
          />
          <span style={{ color: "white" }}>password</span>
          <br />

          <button className="signin">Sign In</button>
          <hr />
          <a href="#">Forgot Password?</a>
        </form>
      </div>
    </div>
  );
};

export default Login;

// https://www.youtube.com/watch?v=mMzhWXr9ass
